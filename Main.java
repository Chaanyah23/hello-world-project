import java.util.*;

public class Main {

    // This is the main method
    public  static void main(String[] args) {

        System.out.println("Main Method:");

        for (int i =1; i <= 10; i++) {
            System.out.println(i + "." + " Hello World");
        }

        // Prints results from the sec method
        sec();

        // Prints results from the three method
        three();

        // Prints results from the three method
        four();

    }

    // This method is called sec
    public  static void sec() {

        System.out.println("\n");
        System.out.println("Sec Method:");

        for (int i =1; i <= 10; i++) {
            for (int j = 1; j <=10; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println("\n");
        }

    }

    public static void three() {

        System.out.println("\n");
        System.out.println("Three Method:");

        try {
            int[] arrays = {54, 20, 65, 777, 14, 54, 23};
            int large_number = 0;

            for (int i = 0; i < arrays.length; i++) {
                if (arrays[i] > large_number) {
                    large_number = arrays[i];
                }
            }
            System.out.println(large_number);

        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Arrys is too large for the index");
        }

    }

    public static void four() {

        System.out.println("\n");
        System.out.println("Four Method:");
        int[][] arrays = {{10, 41, 25, 64, 85, 41, 21, 35, 10, 15, 55, 87, 99}, {55, 69, 78, 84, 10, 25, 45, 30, 24, 45, 65, 77, 100}};

        try {
            for (int i = 0; i < arrays.length; i++) {
                for (int j = 0; j < arrays[i].length; j++) {
                    System.out.print(arrays[i][j] + " ");
                }

            }

        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("\n");
            System.out.println("Array is out of bounds");
        }

    }

}
